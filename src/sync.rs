use crate::bar_print;
use std::{
    fs::{self, File},
    io::{self, BufReader},
};

fn sync_run(out: std::path::PathBuf) -> Result<(), ()> {
    let mut data = vec![];
    let mut handle = curl::easy::Easy::new();
    if handle
        .url("https://anidb.net/api/anime-titles.dat.gz")
        .is_ok()
        && handle.useragent("tani").is_ok()
    {
        let mut transfer = handle.transfer();
        let wf = |x: &[u8]| {
            data.extend_from_slice(x);
            Ok(x.len())
        };
        if transfer.write_function(wf).is_err() || transfer.perform().is_err() {
            return Err(());
        }
    }
    if handle.content_type() == Ok(Some("application/gzip")) {
        if let Ok(mut x) = File::create(out) {
            let mut read = BufReader::new(flate2::read::GzDecoder::new(data.as_slice()));
            if io::copy(&mut read, &mut x).is_ok() {
                return Ok(());
            }
        }
    }
    Err(())
}

pub fn sync(tf: std::path::PathBuf, out: std::path::PathBuf) -> std::thread::JoinHandle<()> {
    std::thread::spawn(move || {
        let last = fs::read_to_string(&tf)
            .ok()
            .and_then(|x| x.parse::<u64>().ok())
            .unwrap_or(0);
        if let Ok(now) = std::time::SystemTime::now()
            .duration_since(std::time::SystemTime::UNIX_EPOCH)
            .map(|x| x.as_secs())
        {
            if last + 60 * 60 * 24 < now {
                bar_print("Syncing...");
                let handle = std::thread::spawn(move || sync_run(out));
                bar_print(if handle.join().is_ok() {
                    _ = fs::write(tf, format!("{}", now));
                    "Sync done!"
                } else {
                    "Sync failed :("
                });
            }
        }
    })
}
