use std::fmt;
use termion::color;

pub struct Keys {
    pub up: char,
    pub down: char,
    pub pdown: char,
    pub pup: char,
    pub quit: char,
    pub delete: char,
    pub search: char,
    pub eup: char,
    pub edown: char,
    pub status: char,
    pub open: char,
    pub add: char,
    pub filter: char,
}

impl Default for Keys {
    fn default() -> Self {
        Keys {
            up: 'k',
            down: 'j',
            pdown: 'f',
            pup: 'b',
            quit: 'q',
            delete: 'D',
            search: '/',
            eup: '+',
            edown: '-',
            status: 's',
            open: 'o',
            add: 'i',
            filter: 't',
        }
    }
}

#[derive(Debug, Clone)]
pub struct Line(pub String, pub u8, pub u16);

impl fmt::Display for Line {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        let s = match self.1 {
            1 => "Completed",
            2 => "Dropped",
            3 => "Plan to Watch",
            4 => "Watching",
            _ => "",
        };
        let w = formatter.width().unwrap_or(100);
        let t = if self.0.len() > w {
            self.0.split_at(w).0
        } else {
            &self.0
        };
        write!(formatter, "{:w$}  {:13}  {: <5}", t, s, self.2)
    }
}

#[derive(Debug, Clone)]
pub struct Item {
    pub id: u32,
    pub line: Line,
}

impl Item {
    pub fn set_status(&mut self, s: Status) {
        self.line.1 = s as u8;
    }

    pub fn set_episode(&mut self, u: u16) {
        self.line.2 = u;
    }
}

#[derive(PartialEq)]
pub enum NavigateResult {
    Quit,
    Select,
    Continue,
}

pub struct Colors {
    pub fg: color::AnsiValue,
    pub bg: color::AnsiValue,
    pub sfg: color::AnsiValue,
    pub sbg: color::AnsiValue,
}

impl Default for Colors {
    fn default() -> Self {
        Colors {
            sfg: color::AnsiValue(0),
            sbg: color::AnsiValue(1),
            fg: color::AnsiValue(1),
            bg: color::AnsiValue(0),
        }
    }
}

#[derive(Copy, Clone)]
pub enum Status {
    Watching = 4,
    Completed = 1,
    Dropped = 2,
    PlanToWatch = 3,
}

impl fmt::Display for Status {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(
            formatter,
            "{}",
            match self {
                Status::PlanToWatch => "Plan to Watch",
                Status::Watching => "Watching",
                Status::Dropped => "Dropped",
                Status::Completed => "Completed",
            }
        )
    }
}

impl TryFrom<char> for Status {
    type Error = ();
    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            'w' => Ok(Status::Watching),
            'c' => Ok(Status::Completed),
            'd' => Ok(Status::Dropped),
            'p' => Ok(Status::PlanToWatch),
            _ => Err(()),
        }
    }
}
